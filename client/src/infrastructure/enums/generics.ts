export enum RequestStatusEnum {
  NONE = -1,
  LOADING = 0,
  SUCCESS = 1,
  ERROR = 2,
}
